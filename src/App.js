import React from 'react';
import logo from './logo.png';
import logo2 from './logo2.png';
import video from './loop.mp4'
import facebook from './facebook.png';
import instagram from './instagram.png';
import whatsapp from './whatsapp.png';
import background from './background.png';
import spotify from './spotify.png';
import './App.scss';

const styles = {
  backgroundImage: `url(${background})`
}

function App() {
  return (
    <div className="immersion-page">
      <header className="immersion-page-landing-header"></header>
      <div className="immersion-page-landing-container">
        <div className="immersion-page-landing-left">
          <video autoPlay muted loop>
              <source src={video} type="video/mp4"></source>
              Your browser does not support the video tag.
          </video>
          <div className="immersion-page-landing-data">
            <div className="immersion-page-landing-data-date">
              <h3>29</h3>
              <div className="separator"></div>
              <h6>Agosto</h6>
            </div>
            <div className="immersion-page-landing-data-logo">
              <img alt="logo" src={logo}></img>
            </div>
          </div>
        </div>
        <div style={styles} className="immersion-page-landing-right">
          <div className="immersion-page-landing-right-input">
            <form className="immersion-page-landing-right-input-form">
              <h2>Registro</h2>
              <input type="text" placeholder="Nombre"></input>
              <input type="text" placeholder="Correo"></input>
              <input type="text" placeholder="Celular"></input>
              <button>Guardar</button>
              <div className="immersion-page-landing-right-timer">
                <span>Faltan: 20 días para la Immersion</span>
                <span>Pronto boletería disponible</span>
              </div>
            </form>
            
          </div>
          <div className="immersion-page-landing-right-social">
            <div className="immersion-page-landing-right-social-links">
              <img alt="logo" src={facebook}></img>
              <img alt="logo" src={whatsapp}></img>
              <img alt="logo" src={instagram}></img>
              <img alt="logo" src={spotify}></img>
            </div>
            <img alt="logo" className="immersion-page-landing-right-social-main" src={logo2}></img>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
